const my_is_posi_neg_t = (nbr) => {
  if (nbr <= 0) {
    return "NEGATIVE";
  }

  return "POSITIF";
};

module.exports = my_is_posi_neg_t;
