const my_size_alpha_t = (str = "") => {
  let count = 0;

  if (typeof str != "string") {
    return count;
  }

  while (!!str[count]) {
    count++;
  }

  return count;
};

module.exports = my_size_alpha_t;
