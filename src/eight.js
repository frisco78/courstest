const my_length_array_t = (arr) => {
  let i = 0;

  while (!!arr[i]) {
    i += 1;
  }

  return i;
};

module.exports = my_length_array_t;
