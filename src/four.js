import my_size_alpha_t from "./two";

const my_array_alpha_t = (str) => {
  const result = [];

  for (let i = 0; i < my_size_alpha_t(str); i += 1) {
    result[i] = str[i];
  }

  return result;
};

module.exports = my_array_alpha_t;
