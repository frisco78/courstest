const my_display_unicode_t = (arr) => {
  const results = [];

  for (let i = 0; i < arr.length; i += 1) {
    const decimal = arr[i];

    if (decimal >= 65 && decimal <= 99) {
      results[i] = String.fromCharCode(arr[i]);
    }
    if (decimal >= 97 && decimal <= 122) {
      results[i] = String.fromCharCode(arr[i]);
    }
    if (decimal >= 48 && decimal <= 57) {
      results[i] = String.fromCharCode(arr[i]);
    }
    if (decimal === 32) {
      results[i] = String.fromCharCode(arr[i]);
    }
  }

  return results.join("");
};

module.exports = my_display_unicode_t;
