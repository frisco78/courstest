import my_display_alpha_t from "./three";
import my_size_alpha_t from "./two";

const my_display_alpha_reverse_t = () => {
  const alpha = my_display_alpha_t();
  let reverseAlpha = "";

  for (let i = my_size_alpha_t(alpha); i > 0; i -= 1) {
    reverseAlpha += alpha[i - 1];
  }

  return reverseAlpha;
};

module.exports = my_display_alpha_reverse_t;
