import my_alpha_number_t from "../src/zero";

describe("my_alpha_number_t Tests", () => {
  it('Should return "undefined" if no argument is given', () => {
    expect(my_alpha_number_t()).toBe("undefined");
  });
  it('Should return "bonjour" if argument "bonjour" is given', () => {
    expect(my_alpha_number_t("bonjour")).toBe("bonjour");
  });
  it('Should return "" if argument "" is given', () => {
    expect(my_alpha_number_t("")).toBe("");
  });
  it('Should return "4" (string) if argument "4" (int) is given', () => {
    expect(my_alpha_number_t(4)).toBe("4");
  });
});
