import my_size_alpha_t from "../src/two";

describe("my_size_alpha_t Tests", () => {
  it('Should return "0" if no argument are given', () => {
    expect(my_size_alpha_t()).toBe(0);
  });
  it('Should return "0" if no argument are given', () => {
    expect(my_size_alpha_t("test")).toBe(4);
  });
  it('Should return "0" if no argument are given', () => {
    expect(my_size_alpha_t(4)).toBe(0);
  });
});
