import sum from "../src/one";

describe("sum Tests", () => {
  it('Should return "3" if argument a is "1" and b is "2"', () => {
    expect(sum(1, 2)).toBe(3);
  });
  it('Should return "0" if argument a or b are not a number', () => {
    expect(sum("test", 1)).toBe(0);
    expect(sum(1, "test")).toBe(0);
  });
  it('Should return "0" if argument a and b are undefined', () => {
    expect(sum()).toBe(0);
  });
});
