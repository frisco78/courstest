import my_display_unicode_t from "../src/nine";

describe("my_display_unicode_t Tests", () => {
  it('Should return "b" if array [66, 121, 49, 32, 98] is given', () => {
    expect(my_display_unicode_t([66, 121, 49, 32, 98])).toBe("By1 b");
  });
  it('Should return "" (empty string) if array with non right code element is given', () => {
    expect(my_display_unicode_t(["15"])).toBe("");
  });
});
