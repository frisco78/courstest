import my_display_alpha_reverse_t from "../src/seven";

describe("my_display_alpha_reverse_t Tests", () => {
  it('Should return "zyxwvutsrqponmlkjihgfedcba" if no argument are given', () => {
    expect(my_display_alpha_reverse_t()).toBe("zyxwvutsrqponmlkjihgfedcba");
  });
  it('Should return "zyxwvutsrqponmlkjihgfedcba" if any argument are given', () => {
    expect(my_display_alpha_reverse_t("test")).toBe(
      "zyxwvutsrqponmlkjihgfedcba"
    );
  });
});
