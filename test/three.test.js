import my_display_alpha_t from "../src/three";

describe("my_display_alpha_t Tests", () => {
  it('Should return "abcdefghijklmnopqrstuvwxyz" if no argument are given', () => {
    expect(my_display_alpha_t()).toBe("abcdefghijklmnopqrstuvwxyz");
  });
  it('Should return "abcdefghijklmnopqrstuvwxyz" if any argument is given', () => {
    expect(my_display_alpha_t("test")).toBe("abcdefghijklmnopqrstuvwxyz");
  });
});
