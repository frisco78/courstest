import fibo from "../src/six";

describe("fibo Tests", () => {
  it('Should return "0" if negative int or 0 is given', () => {
    expect(fibo(-5)).toBe(0);
    expect(fibo(0)).toBe(0);
  });
  it('Should return "1" if "1" or "2" is given', () => {
    expect(fibo(1)).toBe(1);
    expect(fibo(2)).toBe(1);
  });
  it('Should return "55" if 10 is given', () => {
    expect(fibo(10)).toBe(55);
  });
  it('Should throw Error if any non int argument is given', () => {
    function fiboNonInt() {
        return fibo("test")
    }
    expect(fiboNonInt).toThrow(/Maximum call stack size exceeded/);
  });
});
