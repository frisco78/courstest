import my_array_alpha_t from "../src/four";

describe("my_array_alpha_t Tests", () => {
  it('Should return "[]" if no argument are given', () => {
    expect(my_array_alpha_t()).toStrictEqual([]);
  });
  it('Should return "["t","e","s","t"]" if argument "test" is given', () => {
    expect(my_array_alpha_t("test")).toStrictEqual(["t","e","s","t"]);
  });
  it('Should return "[]" if argument "42" (int) is given', () => {
    expect(my_array_alpha_t(42)).toStrictEqual([]);
  });
});
