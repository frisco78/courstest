import my_is_posi_neg_t from "../src/five";

describe("my_is_posi_neg_t Tests", () => {
  it('Should return "NEGATIVE" if negative int is given', () => {
    expect(my_is_posi_neg_t(-5)).toBe("NEGATIVE");
  });
  it('Should return "POSITIF" if positive int is given', () => {
    expect(my_is_posi_neg_t(5)).toBe("POSITIF");
  });
  it('Should return "NEGATIVE" if "0" is given', () => {
    expect(my_is_posi_neg_t(0)).toBe("NEGATIVE");
  });
  it('Should return "POSITIF" if any non number argument is given', () => {
    expect(my_is_posi_neg_t()).toBe("POSITIF");
  });
});
