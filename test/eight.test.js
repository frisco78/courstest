import my_length_array_t from "../src/eight";

describe("my_length_array_t Tests", () => {
  it('Should return "0" if empty array is given', () => {
    expect(my_length_array_t([])).toBe(0);
  });
  it('Should return "4" if array with 4 elements is given', () => {
    expect(my_length_array_t(["t","e","s","t"])).toBe(4);
  });
  it('Should return "4" if a 4 character string is given', () => {
    expect(my_length_array_t("test")).toBe(4);
  });
  it('Should return "0" if no argument are given', () => {
    function myLengthArrayTNoArgs() {
        return my_length_array_t()
    }
    expect(myLengthArrayTNoArgs).toThrow("Cannot read properties of undefined (reading '0')");
  });
});
